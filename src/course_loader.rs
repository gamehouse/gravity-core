use crate::Course;
use gdnative::api::{PackedScene, Resource};
use gdnative::prelude::*;

#[derive(NativeClass, Default, Debug)]
#[inherit(Resource)]
pub struct CourseLoader {
    #[property]
    course_name: String,

    #[property]
    packed_course: Option<Ref<PackedScene>>,
}

#[methods]
impl CourseLoader {
    fn new(_owner: &Resource) -> Self {
        Default::default()
    }

    pub fn course_name(&self) -> &str {
        self.course_name.as_str()
    }

    pub fn load_course(&self) -> Instance<Course, Unique> {
        let packed_course = unsafe { self.packed_course.as_ref().unwrap().assume_safe() };
        let course = packed_course
            .instance(PackedScene::GEN_EDIT_STATE_DISABLED)
            .expect("Need to be able to instance a packed course");
        let course = unsafe { course.assume_unique() };
        course.cast_instance().unwrap()
    }
}
