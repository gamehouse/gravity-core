use crate::{GetTypedNode, Settings, SettingsMenu};
use gdnative::api::{Button, CanvasLayer};
use gdnative::prelude::*;
use std::cell::RefCell;
use std::rc::Rc;

#[derive(NativeClass, Default, Debug)]
#[inherit(CanvasLayer)]
#[register_with(Self::register)]
pub struct PauseMenu {
    resume: Option<Ref<Button>>,
    settings_menu: Option<Instance<SettingsMenu, Shared>>,
    settings: Rc<RefCell<Settings>>,
}

#[methods]
impl PauseMenu {
    fn register(builder: &ClassBuilder<Self>) {
        builder.add_signal(Signal {
            name: "restart_hole",
            args: &[],
        });
        builder.add_signal(Signal {
            name: "exit",
            args: &[],
        });
        builder.add_signal(Signal {
            name: "unpause",
            args: &[],
        });
    }

    fn new(_owner: &CanvasLayer) -> Self {
        Default::default()
    }

    pub fn set_settings(&mut self, settings: Rc<RefCell<Settings>>) {
        self.settings = settings.clone();
        if let Some(settings_menu) = self.settings_menu.as_ref() {
            let settings_menu = unsafe { settings_menu.assume_safe() };
            settings_menu
                .map_mut(|script, _| script.set_settings(settings))
                .unwrap();
        }
    }

    #[export]
    pub fn focus(&self, _owner: TRef<CanvasLayer>) {
        let resume = self.resume.unwrap();
        let resume = unsafe { resume.assume_safe() };
        resume.grab_focus();
    }

    #[export]
    fn _ready(&mut self, owner: TRef<CanvasLayer>) {
        let node: TRef<Node> = owner.upcast();
        self.resume = Some(unsafe { node.get_claimed_node("Panel/VBoxContainer/Resume") });

        let settings_menu: RefInstance<SettingsMenu, _> =
            unsafe { node.get_typed_instance("SettingsMenu") };
        settings_menu
            .map_mut(|script, _| script.set_settings(self.settings.clone()))
            .unwrap();
        self.settings_menu = Some(settings_menu.claim());
    }

    #[export]
    fn restart_hole(&self, owner: TRef<CanvasLayer>) {
        owner.emit_signal("unpause", &[]);
        owner.emit_signal("restart_hole", &[]);
    }

    #[export]
    fn exit(&self, owner: TRef<CanvasLayer>) {
        owner.emit_signal("unpause", &[]);
        owner.emit_signal("exit", &[]);
    }

    #[export]
    fn resume(&self, owner: TRef<CanvasLayer>) {
        owner.emit_signal("unpause", &[]);
    }

    #[export]
    fn show_settings(&self, _owner: TRef<CanvasLayer>) {
        let settings_menu = unsafe { self.settings_menu.as_ref().unwrap().assume_safe() };
        settings_menu
            .map(|script, base| script.show(&base))
            .unwrap();
    }
}
