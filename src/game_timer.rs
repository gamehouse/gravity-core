use std::fmt;
use std::ops::{Deref, DerefMut};
use std::time::{Duration, Instant};

/// A wrapper for Duration that gives it a convenient Display implementation.
#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Runtime(Duration);

impl Deref for Runtime {
    type Target = Duration;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl DerefMut for Runtime {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}

impl From<Duration> for Runtime {
    fn from(duration: Duration) -> Self {
        Self(duration)
    }
}

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct PausedError;

impl fmt::Display for PausedError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "PausedError: Can not pause an already-paused timer")
    }
}

impl std::error::Error for PausedError {}

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct NotPausedError;

impl fmt::Display for NotPausedError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "NotPausedError: Can not unpause an already-unpaused timer"
        )
    }
}

impl std::error::Error for NotPausedError {}

/// Simple time-tracking struct to manage running gametime, including managing pausing.
///
/// Also has a convenient Display implementation for showing in a HUD.
#[derive(Debug, PartialEq, Eq, Hash, PartialOrd, Ord)]
pub struct GameTimer {
    start_time: Instant,
    total_paused_time: Duration,
    paused_time: Option<Instant>,
}

impl Default for GameTimer {
    fn default() -> Self {
        GameTimer {
            start_time: Instant::now(),
            total_paused_time: Duration::default(),
            paused_time: None,
        }
    }
}

impl GameTimer {
    pub fn paused(&self) -> bool {
        self.paused_time.is_some()
    }

    pub fn pause(&mut self) -> Result<(), PausedError> {
        if self.paused() {
            Err(PausedError)
        } else {
            self.paused_time = Some(Instant::now());
            Ok(())
        }
    }

    pub fn unpause(&mut self) -> Result<(), NotPausedError> {
        match self.paused_time {
            Some(paused_time) => {
                self.total_paused_time += paused_time.elapsed();
                self.paused_time = None;
                Ok(())
            }
            None => Err(NotPausedError),
        }
    }

    /// Get the current runtime.
    ///
    /// This takes into account whether the game timer is currently paused or not.
    pub fn runtime(&self) -> Runtime {
        Runtime(match self.paused_time {
            Some(paused_time) => paused_time - self.start_time - self.total_paused_time,
            None => self.start_time.elapsed() - self.total_paused_time,
        })
    }
}

impl fmt::Display for Runtime {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let total_milliseconds = self.as_millis() as u64;
        let total_hours = total_milliseconds / 3600000;
        let total_minutes = total_milliseconds / 60000;
        let total_seconds = total_milliseconds / 1000;

        let hours = total_hours;
        let minutes = total_minutes % 60;
        let seconds = total_seconds % 60;
        let milliseconds = total_milliseconds % 1000;

        if total_hours > 0 {
            write!(f, "{:02}:", hours)?;
        }
        if total_minutes > 0 {
            write!(f, "{:02}:", minutes)?;
        }

        write!(f, "{:02}.{:03}", seconds, milliseconds)
    }
}
