use gdnative::api::Node;
use gdnative::prelude::*;

/// A very simple class that emits its time_tick signal every _process frame, used for running
/// processes that should be run during a paused tree, such as keeping the HUD current.
#[derive(NativeClass, Debug, Default, Copy, Clone)]
#[inherit(Node)]
#[register_with(Self::register)]
pub struct PausedEmitter;

#[methods]
impl PausedEmitter {
    fn register(builder: &ClassBuilder<Self>) {
        builder.add_signal(Signal {
            name: "process",
            args: &[],
        });
    }

    fn new(_owner: &Node) -> Self {
        PausedEmitter::default()
    }

    #[export]
    fn _process(&self, owner: TRef<Node>, _delta: f32) {
        owner.emit_signal("process", &[]);
    }
}
