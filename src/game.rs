use crate::{Course, CourseLoader, GetTypedNode, MainMenu, OrphanInstanceWatcher, Settings};
use gdnative::api::Node;
use gdnative::nativescript::Map;
use gdnative::prelude::*;
use std::cell::RefCell;
use std::rc::Rc;

#[derive(NativeClass, Default)]
#[inherit(Node)]
pub struct Game {
    main_menu: Option<OrphanInstanceWatcher<MainMenu>>,
    course: Option<Instance<Course, Shared>>,
    settings: Rc<RefCell<Settings>>,
}

#[methods]
impl Game {
    fn new(_owner: &Node) -> Self {
        Default::default()
    }

    fn enter_main_menu(&mut self, owner: TRef<Node>) {
        let main_menu = self.main_menu.as_ref().unwrap();
        let parent = {
            let main_menu = unsafe { main_menu.base().assume_safe() };
            main_menu.get_parent()
        };
        // We do this here to make sure we don't risk main_menu violate the safe aliasing with some
        // signal catcher.
        if parent == None {
            owner.add_child(main_menu.base(), false);
        }

        if let Some(course) = self.course.as_ref() {
            let course = unsafe { course.base().assume_safe() };
            course.queue_free();
            self.course = None;
        }
        let main_menu = unsafe { main_menu.assume_safe() };
        main_menu.map(MainMenu::setup).unwrap();
    }

    #[export]
    fn _ready(&mut self, owner: TRef<Node>) {
        let node: TRef<Node> = owner.upcast();
        let main_menu: RefInstance<MainMenu, Shared> =
            unsafe { node.get_typed_instance("MainMenu") };
        main_menu
            .map_mut(|main_menu, main_menu_base| {
                main_menu.set_settings(self.settings.clone());
                main_menu_base
                    .connect("exit", owner, "exit_game", VariantArray::new_shared(), 0)
                    .unwrap();
                main_menu_base
                    .connect(
                        "start_course",
                        owner,
                        "start_course",
                        VariantArray::new_shared(),
                        0,
                    )
                    .unwrap();
            })
            .unwrap();
        self.main_menu = Some(main_menu.claim().into());
        self.enter_main_menu(owner);
    }

    #[export]
    fn exit_game(&self, owner: TRef<Node>) {
        let tree = unsafe { owner.get_tree().unwrap().assume_safe() };
        tree.quit(-1);
    }

    #[export]
    fn start_course(&mut self, owner: TRef<Node>, course_loader: Instance<CourseLoader, Shared>) {
        let course_loader = unsafe { course_loader.assume_safe() };
        let course = course_loader
            .script()
            .map(CourseLoader::load_course)
            .unwrap();
        let course = course.into_shared();
        owner.add_child(course.base(), false);
        let course = unsafe { course.assume_safe() };
        course
            .map_mut(|script, base| {
                base.connect(
                    "victory",
                    owner,
                    "exit_course",
                    VariantArray::new_shared(),
                    0,
                )
                .unwrap();
                base.connect("exit", owner, "exit_course", VariantArray::new_shared(), 0)
                    .unwrap();
                script.set_settings(self.settings.clone());
            })
            .unwrap();
        self.course = Some(course.claim());
        owner.remove_child(self.main_menu.as_ref().unwrap().base());
    }

    #[export]
    fn exit_course(&mut self, owner: TRef<Node>) {
        self.enter_main_menu(owner);
    }
}
