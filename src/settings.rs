#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum CameraType {
    Fixed,
    Rotating { rotation_multiplier: u8 },
}

impl Default for CameraType {
    fn default() -> Self {
        CameraType::Rotating {
            rotation_multiplier: 10,
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum ControlsAlignment {
    Camera,
    Player,
}

impl Default for ControlsAlignment {
    fn default() -> Self {
        ControlsAlignment::Camera
    }
}

/// Simple settings structure, shared via a Rc<RefCell> through the game.
#[derive(Debug, Clone, Copy, PartialEq, PartialOrd)]
pub struct Settings {
    camera_type: CameraType,
    translation_multiplier: u8,
    controls_alignment: ControlsAlignment,
    gravpack_acceleration: f32,
    gravpack_shot_sensitivity: f32,
}

impl Default for Settings {
    fn default() -> Self {
        Settings {
            camera_type: Default::default(),
            controls_alignment: Default::default(),
            translation_multiplier: 10,
            gravpack_acceleration: 500.0,
            gravpack_shot_sensitivity: 500.0,
        }
    }
}

impl Settings {
    pub fn camera_type(&self) -> CameraType {
        self.camera_type
    }

    pub fn controls_alignment(&self) -> ControlsAlignment {
        self.controls_alignment
    }

    pub fn translation_multiplier(&self) -> u8 {
        self.translation_multiplier
    }

    pub fn gravpack_acceleration(&self) -> f32 {
        self.gravpack_acceleration
    }

    pub fn gravpack_shot_sensitivity(&self) -> f32 {
        self.gravpack_shot_sensitivity
    }

    pub fn set_camera_type(&mut self, camera_type: CameraType) {
        self.camera_type = camera_type;
    }

    pub fn set_controls_alignment(&mut self, controls_alignment: ControlsAlignment) {
        self.controls_alignment = controls_alignment
    }

    pub fn set_translation_multiplier(&mut self, translation_multiplier: u8) {
        self.translation_multiplier = translation_multiplier;
    }

    pub fn set_gravpack_acceleration(&mut self, value: f32) {
        self.gravpack_acceleration = value;
    }

    pub fn set_gravpack_shot_sensitivity(&mut self, value: f32) {
        self.gravpack_shot_sensitivity = value;
    }
}
