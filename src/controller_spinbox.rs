use gdnative::api::{Input, LineEdit, SpinBox};
use gdnative::prelude::*;

/// A SpinBox class that can be operated upon via the controller.
#[derive(NativeClass, Default, Debug)]
#[inherit(SpinBox)]
pub struct ControllerSpinbox {
    line_edit: Option<Ref<LineEdit>>,
}

#[methods]
impl ControllerSpinbox {
    fn new(_owner: &SpinBox) -> Self {
        Default::default()
    }

    #[export]
    pub fn _ready(&mut self, owner: TRef<SpinBox>) {
        self.line_edit = Some(owner.get_line_edit().unwrap());
    }

    #[export]
    pub fn _process(&self, owner: TRef<SpinBox>, _delta: f32) {
        let line_edit = unsafe { self.line_edit.unwrap().assume_safe() };
        if line_edit.has_focus() {
            let input = Input::godot_singleton();
            let step = if input.is_action_pressed("ui_accept") {
                owner.step() * 25.0
            } else {
                owner.step()
            };
            let value = owner.value();
            let value = if input.is_action_just_pressed("ui_left") {
                Some(value - step)
            } else if input.is_action_just_pressed("ui_right") {
                Some(value + step)
            } else {
                None
            };
            if let Some(value) = value {
                owner.set_value(value);
            }
        }
    }
}
