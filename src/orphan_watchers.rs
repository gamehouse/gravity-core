use gdnative::prelude::*;
use std::fmt;
use std::ops::{Deref, DerefMut};

/** A simple wrapper class around Instance to allow auto-dropping when the parent drops this and
 * the node doesn't have a parent.
 */
pub struct OrphanInstanceWatcher<C>
where
    C: NativeClass,
    C::Base: GodotObject<RefKind = ManuallyManaged> + SubClass<Node>,
{
    instance: Instance<C, Shared>,
}

impl<C> Deref for OrphanInstanceWatcher<C>
where
    C: NativeClass,
    C::Base: GodotObject<RefKind = ManuallyManaged> + SubClass<Node>,
{
    type Target = Instance<C, Shared>;

    fn deref(&self) -> &Self::Target {
        &self.instance
    }
}

impl<C> DerefMut for OrphanInstanceWatcher<C>
where
    C: NativeClass,
    C::Base: GodotObject<RefKind = ManuallyManaged> + SubClass<Node>,
{
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.instance
    }
}

impl<C> From<Instance<C, Shared>> for OrphanInstanceWatcher<C>
where
    C: NativeClass,
    C::Base: GodotObject<RefKind = ManuallyManaged> + SubClass<Node>,
{
    fn from(instance: Instance<C, Shared>) -> Self {
        OrphanInstanceWatcher { instance }
    }
}

impl<C> fmt::Debug for OrphanInstanceWatcher<C>
where
    C: NativeClass + fmt::Debug,
    C::UserData: fmt::Debug,
    C::Base: GodotObject<RefKind = ManuallyManaged> + SubClass<Node> + fmt::Debug,
{
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("OrphanInstanceWatcher")
            .field("instance", &self.instance)
            .finish()
    }
}

impl<C> Drop for OrphanInstanceWatcher<C>
where
    C: NativeClass,
    C::Base: GodotObject<RefKind = ManuallyManaged> + SubClass<Node>,
{
    fn drop(&mut self) {
        if let Some(instance) = unsafe { self.instance.base().assume_safe_if_sane() } {
            let instance: TRef<Node> = instance.upcast();
            if instance.get_parent() == None {
                let instance = unsafe { instance.assume_unique() };
                instance.free();
            }
        }
    }
}
