use crate::GetTypedNode;
use euclid::Angle;
use gdnative::api::{Label, Node2D, ProgressBar};
use gdnative::prelude::*;
use std::f64;

#[derive(NativeClass, Debug)]
#[inherit(Node2D)]
#[register_with(Self::register)]
pub struct GravpackShotOverlay {
    progress_bar: Option<Ref<ProgressBar>>,
    force_pivot: Option<Ref<Node2D>>,
    force_label: Option<Ref<Label>>,
    rotation: Angle<f32>,
    camera_rotation: Angle<f32>,
    max_value: f32,
    value: f32,
    sensitivity: f32,
}

impl Default for GravpackShotOverlay {
    fn default() -> Self {
        GravpackShotOverlay {
            progress_bar: None,
            force_pivot: None,
            force_label: None,
            rotation: Angle::zero(),
            camera_rotation: Angle::zero(),
            max_value: 0.0,
            value: 0.0,
            sensitivity: 500.0,
        }
    }
}

#[methods]
impl GravpackShotOverlay {
    fn register(builder: &ClassBuilder<Self>) {
        builder.add_signal(Signal {
            name: "release",
            args: &[SignalArgument {
                name: "strength",
                default: Variant::from_vector2(&Vector2::zero()),
                export_info: ExportInfo::new(VariantType::Vector2),
                usage: PropertyUsage::DEFAULT,
            }],
        });
    }

    fn new(_owner: &Node2D) -> Self {
        Default::default()
    }

    pub fn set_rotation(&mut self, rotation: Angle<f32>) {
        self.rotation = rotation;
    }

    pub fn set_camera_rotation(&mut self, camera_rotation: Angle<f32>) {
        self.camera_rotation = camera_rotation;
    }

    pub fn set_max_value(&mut self, max_value: f32) {
        self.max_value = max_value;
    }

    pub fn set_value(&mut self, value: f32) {
        self.value = value;
    }

    pub fn set_sensitivity(&mut self, sensitivity: f32) {
        self.sensitivity = sensitivity;
    }

    #[export]
    fn _ready(&mut self, owner: TRef<Node2D>) {
        let node: TRef<Node> = owner.upcast();
        self.progress_bar = Some(unsafe { node.get_claimed_node("ProgressBar") });
        self.force_pivot = Some(unsafe { node.get_claimed_node("ProgressBar/ForcePivot") });
        self.force_label = Some(unsafe { node.get_claimed_node("ProgressBar/ForcePivot/Force") });
    }

    #[export]
    fn _process(&mut self, owner: TRef<Node2D>, delta: f32) {
        let input = Input::godot_singleton();
        let input_vector = Vector2::new(
            input.get_action_strength("move_right") as f32
                - input.get_action_strength("move_left") as f32,
            input.get_action_strength("move_down") as f32
                - input.get_action_strength("move_up") as f32,
        )
        .with_max_length(1.0)
        .rotated(self.rotation);

        // progress bar value
        let progress_bar = unsafe { self.progress_bar.unwrap().assume_safe() };

        let input_length = input_vector.length().min(1.0);

        progress_bar.set_value((input_length * 100.0) as f64);

        let input_angle;
        if input_vector == Vector2::zero() {
            // Reset to 0 if the player centers the stick.
            self.value = 0.0;
            input_angle = Angle::zero();
        } else {
            // Get strength in range [-1.0, 1.0]
            let strength = input_length * 2.0 - 1.0;
            self.value += strength * self.sensitivity * delta;
            self.value = self.value.max(0.0).min(self.max_value);

            // Progress bar angle
            input_angle = Vector2::new(1.0, 0.0).angle_to(input_vector);
            progress_bar.set_rotation(input_angle.get() as f64);
            let force_pivot = unsafe { self.force_pivot.unwrap().assume_safe() };
            force_pivot.set_global_rotation(self.camera_rotation.radians as f64);
        }

        let force_label = unsafe { self.force_label.unwrap().assume_safe() };
        force_label.set_text(format!("{:.0}", self.value));

        if !input.is_action_pressed("gravpack_shot") {
            let velocity = Vector2::new(self.value, 0.0).rotated(input_angle);
            owner.emit_signal("release", &[Variant::from_vector2(&velocity)]);
        }
    }
}
