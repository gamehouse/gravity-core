use crate::{GameTimer, GetTypedNode, Hole, Settings};
use gdnative::api::Node;
use gdnative::prelude::*;
use std::cell::RefCell;
use std::rc::Rc;

#[derive(Default, NativeClass)]
#[inherit(Node)]
#[register_with(Self::register)]
pub struct Course {
    current_hole_index: usize,
    current_hole: Option<Instance<Hole, Shared>>,
    holes: Vec<Ref<PackedScene, Unique>>,
    timer: GameTimer,
    settings: Rc<RefCell<Settings>>,
    course_time: Option<Ref<Label>>,
}

#[methods]
impl Course {
    fn register(builder: &ClassBuilder<Self>) {
        builder.add_signal(Signal {
            name: "victory",
            args: &[],
        });
        builder.add_signal(Signal {
            name: "exit",
            args: &[],
        });
        builder
            .add_property::<VariantArray>("holes")
            .with_setter(Course::set_holes)
            .done();
    }

    fn new(_owner: &Node) -> Self {
        Default::default()
    }

    pub fn set_settings(&mut self, settings: Rc<RefCell<Settings>>) {
        self.settings = settings.clone();
        if let Some(hole) = self.current_hole.as_ref() {
            let hole = unsafe { hole.assume_safe() };
            hole.map_mut(|hole, _| hole.set_settings(settings.clone()))
                .unwrap();
        }
    }

    fn set_holes(&mut self, _owner: TRef<Node>, value: VariantArray) {
        for scene in value.into_iter() {
            let hole: Option<Ref<PackedScene>> = scene.try_to_object();
            if let Some(hole) = hole {
                let hole = unsafe { hole.assume_unique() };
                self.holes.push(hole);
            }
        }
    }

    fn unload_hole(&mut self, _owner: &Node) {
        if let Some(hole) = self.current_hole.as_ref() {
            let hole = unsafe { hole.assume_safe() };
            hole.base().queue_free();
            self.current_hole = None;
        }
    }

    fn load_hole(&mut self, owner: TRef<Node>, hole: usize) {
        if hole >= self.holes.len() {
            return;
        }
        self.unload_hole(&owner);
        let current_hole = self.holes[hole]
            .instance(PackedScene::GEN_EDIT_STATE_DISABLED)
            .expect("Could not load the given hole!");
        owner.add_child(current_hole, false);
        let current_hole = unsafe { current_hole.assume_safe() };
        let current_hole: RefInstance<Hole, _> = current_hole.cast_instance().unwrap();
        current_hole
            .map_mut(|script, base| {
                script.set_settings(self.settings.clone());
                base.connect(
                    "victory",
                    owner,
                    "load_next_hole",
                    VariantArray::new_shared(),
                    0,
                )
                .expect("Need to be able to connect to hole victory");
                base.connect("pause", owner, "pause", VariantArray::new_shared(), 0)
                    .expect("Need to be able to connect to hole victory");
                base.connect("unpause", owner, "unpause", VariantArray::new_shared(), 0)
                    .expect("Need to be able to connect to hole victory");
                base.connect("exit", owner, "exit", VariantArray::new_shared(), 0)
                    .expect("Need to be able to connect to hole victory");
            })
            .unwrap();
        self.current_hole = Some(current_hole.claim());
    }

    #[export]
    fn _ready(&mut self, owner: TRef<Node>) {
        // Reconstruct timer to make sure it's only set to run on actual course start
        self.timer = Default::default();
        self.load_hole(owner, 0);
        let node: TRef<Node> = owner.upcast();
        self.course_time = Some(unsafe { node.get_claimed_node("Hud/CourseTime") });
    }

    #[export]
    fn run_time(&mut self, _owner: TRef<Node>) {
        if !self.timer.paused() {
            // Subtract the duration, capping it at zero
            let runtime = self.timer.runtime();
            let course_time = unsafe { self.course_time.unwrap().assume_safe() };
            course_time.set_text(runtime.to_string());
        }
    }

    #[export]
    fn load_next_hole(&mut self, owner: TRef<Node>) {
        self.current_hole_index += 1;
        if self.current_hole_index >= self.holes.len() {
            owner.emit_signal("victory", &[]);
        } else {
            self.load_hole(owner, self.current_hole_index);
        }
    }

    #[export]
    fn exit(&self, owner: TRef<Node>) {
        owner.emit_signal("exit", &[]);
    }

    #[export]
    fn pause(&mut self, _owner: TRef<Node>) {
        self.timer.pause().unwrap();
    }

    #[export]
    fn unpause(&mut self, _owner: TRef<Node>) {
        self.timer.unpause().unwrap();
    }
}
