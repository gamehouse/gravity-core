use crate::GetTypedNode;
use gdnative::api::{AudioStreamOGGVorbis, AudioStreamPlayer, Node};
use gdnative::prelude::*;
use rand::seq::SliceRandom;
use std::iter::Cycle;
use std::vec::IntoIter;

#[derive(Default, NativeClass)]
#[inherit(Node)]
#[register_with(Self::register)]
pub struct MusicShufflePlayer {
    // Just a little more readable than Option<Box<dyn Iterator<Item=Ref<AudioStreamOGGVorbis>>>>
    tracks: Option<Cycle<IntoIter<Ref<AudioStreamOGGVorbis>>>>,

    player: Option<Ref<AudioStreamPlayer>>,
}

#[methods]
impl MusicShufflePlayer {
    fn register(builder: &ClassBuilder<Self>) {
        builder
            .add_property::<VariantArray>("tracks")
            .with_setter(MusicShufflePlayer::set_tracks)
            .done();
    }

    fn new(_owner: &Node) -> Self {
        Default::default()
    }

    #[export]
    fn _ready(&mut self, owner: TRef<Node>) {
        self.player = Some(unsafe { owner.get_claimed_node("AudioStreamPlayer") });
        self.play_next_track(owner);
    }

    #[export]
    fn play_next_track(&mut self, _owner: TRef<Node>) {
        let player = unsafe { self.player.unwrap().assume_safe() };
        player.set_stream(self.tracks.as_mut().unwrap().next().unwrap());
        player.play(0.0);
    }

    /** Set the tracks from the VariantArray, shuffling it, and setting it to a cycle iterator.
     */
    fn set_tracks(&mut self, _owner: TRef<Node>, value: VariantArray) {
        let mut tracks: Vec<Ref<AudioStreamOGGVorbis>> = Vec::new();
        for scene in value.into_iter() {
            let track: Option<Ref<AudioStreamOGGVorbis>> = scene.try_to_object();
            if let Some(track) = track {
                tracks.push(track);
            }
        }
        let mut rng = rand::thread_rng();
        tracks.shuffle(&mut rng);
        self.tracks = Some(tracks.into_iter().cycle());
    }
}
