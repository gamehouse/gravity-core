use crate::settings::{CameraType, ControlsAlignment, Settings};
use crate::GetTypedNode;
use gdnative::api::{Button, CheckBox, Panel, SpinBox};
use gdnative::prelude::*;
use std::cell::RefCell;
use std::rc::Rc;

#[derive(NativeClass, Default, Debug)]
#[inherit(Panel)]
#[register_with(Self::register)]
pub struct SettingsMenu {
    settings: Rc<RefCell<Settings>>,
    cancel_button: Option<Ref<Button>>,
    fixed_camera: Option<Ref<CheckBox>>,
    player_aligned: Option<Ref<CheckBox>>,
    camera_rotation: Option<Ref<SpinBox>>,
    camera_position: Option<Ref<SpinBox>>,
    gravpack_acceleration: Option<Ref<SpinBox>>,
    gravpack_shot_sensitivity: Option<Ref<SpinBox>>,
}

#[methods]
impl SettingsMenu {
    fn register(builder: &ClassBuilder<Self>) {
        builder.add_signal(Signal {
            name: "exit",
            args: &[],
        });
    }

    fn new(_owner: &Panel) -> Self {
        Default::default()
    }

    pub fn set_settings(&mut self, settings: Rc<RefCell<Settings>>) {
        self.settings = settings;
    }

    pub fn show(&self, owner: &Panel) {
        let cancel_button = unsafe { self.cancel_button.unwrap().assume_safe() };
        owner.set_visible(true);
        cancel_button.grab_focus();

        let settings = self.settings.borrow();

        // Set panel based on settings.
        let fixed_camera = unsafe { self.fixed_camera.unwrap().assume_safe() };
        let player_aligned = unsafe { self.player_aligned.unwrap().assume_safe() };
        let camera_rotation = unsafe { self.camera_rotation.unwrap().assume_safe() };
        let camera_position = unsafe { self.camera_position.unwrap().assume_safe() };
        let gravpack_acceleration = unsafe { self.gravpack_acceleration.unwrap().assume_safe() };
        let gravpack_shot_sensitivity =
            unsafe { self.gravpack_shot_sensitivity.unwrap().assume_safe() };
        camera_position.set_value(settings.translation_multiplier().into());
        gravpack_acceleration.set_value(settings.gravpack_acceleration() as f64);
        gravpack_shot_sensitivity.set_value(settings.gravpack_shot_sensitivity() as f64);

        fixed_camera.set_pressed(matches!(settings.camera_type(), CameraType::Fixed));
        player_aligned.set_pressed(matches!(
            settings.controls_alignment(),
            ControlsAlignment::Player
        ));
        if let CameraType::Rotating {
            rotation_multiplier,
        } = settings.camera_type()
        {
            camera_rotation.set_value(rotation_multiplier.into());
        } else {
            camera_rotation.set_value(10.0);
            camera_position.set_value(10.0);
        }
    }

    #[export]
    fn _ready(&mut self, owner: TRef<Panel>) {
        let node: TRef<Node> = owner.upcast();
        self.fixed_camera = Some(unsafe { node.get_claimed_node("VBoxContainer/FixedCamera") });
        self.player_aligned = Some(unsafe { node.get_claimed_node("VBoxContainer/PlayerAligned") });
        self.camera_rotation =
            Some(unsafe { node.get_claimed_node("VBoxContainer/RotationMultiplier/SpinBox") });
        self.camera_position =
            Some(unsafe { node.get_claimed_node("VBoxContainer/PositionMultiplier/SpinBox") });
        self.gravpack_acceleration =
            Some(unsafe { node.get_claimed_node("VBoxContainer/GravpackAcceleration/SpinBox") });
        self.gravpack_shot_sensitivity =
            Some(unsafe { node.get_claimed_node("VBoxContainer/GravpackShotSensitivity/SpinBox") });
        self.cancel_button = Some(unsafe { node.get_claimed_node("VBoxContainer/Cancel") });
    }

    #[export]
    fn exit(&self, owner: TRef<Panel>) {
        owner.set_visible(false);
        owner.emit_signal("exit", &[]);
    }

    #[export]
    fn save(&self, owner: TRef<Panel>) {
        let mut settings = self.settings.borrow_mut();
        let fixed_camera = unsafe { self.fixed_camera.unwrap().assume_safe() };
        let player_aligned = unsafe { self.player_aligned.unwrap().assume_safe() };
        let camera_position = unsafe { self.camera_position.unwrap().assume_safe() };
        let gravpack_acceleration = unsafe { self.gravpack_acceleration.unwrap().assume_safe() };
        let gravpack_shot_sensitivity =
            unsafe { self.gravpack_shot_sensitivity.unwrap().assume_safe() };
        let translation_multiplier = camera_position.value() as u8;
        settings.set_gravpack_acceleration(gravpack_acceleration.value() as f32);
        settings.set_gravpack_shot_sensitivity(gravpack_shot_sensitivity.value() as f32);
        settings.set_translation_multiplier(translation_multiplier);
        if player_aligned.is_pressed() {
            settings.set_controls_alignment(ControlsAlignment::Player);
        } else {
            settings.set_controls_alignment(ControlsAlignment::Camera);
        }
        if fixed_camera.is_pressed() {
            settings.set_camera_type(CameraType::Fixed);
        } else {
            let camera_rotation = unsafe { self.camera_rotation.unwrap().assume_safe() };
            let rotation_multiplier = camera_rotation.value() as u8;
            settings.set_camera_type(CameraType::Rotating {
                rotation_multiplier,
            });
        }
        self.exit(owner);
    }
}
