use crate::settings::{CameraType, ControlsAlignment, Settings};
use crate::{GetTypedNode, GravpackShotOverlay, OrphanInstanceWatcher};
use euclid::Angle;
use gdnative::api::{
    AnimationPlayer, Camera2D, KinematicBody2D, Label, Node2D, RayCast2D, Sprite, Timer,
};
use gdnative::prelude::*;
use std::cell::RefCell;
use std::f32;
use std::f64;
use std::rc::Rc;

const ROTATION_MULTIPLIER: f32 = 10.0;

#[derive(Debug, PartialEq, Eq, Clone, Copy, Hash)]
enum Animation {
    Idle,
    Walk,
    Run,
    Jump,
    Fall,
}

impl Default for Animation {
    fn default() -> Self {
        Animation::Idle
    }
}

impl Animation {
    fn to_str(self) -> &'static str {
        match self {
            Animation::Idle => "idle",
            Animation::Walk => "walk",
            Animation::Run => "run",
            Animation::Jump => "jump",
            Animation::Fall => "fall",
        }
    }
}

#[derive(NativeClass, Debug)]
#[inherit(KinematicBody2D)]
#[register_with(Self::register)]
pub struct Player {
    #[property(default = 500.0)]
    jump_speed: f32,

    #[property(default = 150.0)]
    run_speed: f32,

    #[property(default = 0.0)]
    gravpack_fuel: f32,

    current_animation: Animation,
    velocity: Vector2,

    /// Net acceleration caused by core pull.
    net_core_acceleration: Vector2,

    floor_detector: Option<Ref<RayCast2D>>,
    sprite: Option<Ref<Sprite>>,
    animation_player: Option<Ref<AnimationPlayer>>,
    camera: Option<Ref<Camera2D>>,
    gravpack_shot_overlay: Option<OrphanInstanceWatcher<GravpackShotOverlay>>,
    gravpack_shot_reground_timeout: Option<Ref<Timer>>,

    /// Used to reparent an airborn player to the base level.  A grounded player is set to be the
    /// child of their ground so that they can move properly relative to that child with that
    /// child's rotation and other movement.
    air_parent: Option<Ref<Node>>,

    /// Used to keep track of whether the player is grounded.  This is just to be more efficient
    /// and avoid unnecessary redundant node checks.
    grounded: bool,

    settings: Rc<RefCell<Settings>>,
    victory_arrow: Option<Ref<Sprite>>,
    velocity_arrow: Option<Ref<Node2D>>,
    velocity_label: Option<Ref<Label>>,
    velocity_pivot: Option<Ref<Node2D>>,

    in_jump: bool,
}

impl Default for Player {
    fn default() -> Self {
        Player {
            jump_speed: 500.0,
            run_speed: 150.0,
            gravpack_fuel: 0.0,
            current_animation: Animation::default(),
            velocity: Vector2::zero(),
            net_core_acceleration: Vector2::zero(),
            floor_detector: None,
            sprite: None,
            victory_arrow: None,
            velocity_arrow: None,
            velocity_label: None,
            velocity_pivot: None,
            animation_player: None,
            camera: None,
            gravpack_shot_overlay: None,
            gravpack_shot_reground_timeout: None,
            air_parent: None,
            grounded: false,
            settings: Default::default(),
            in_jump: false,
        }
    }
}

#[methods]
impl Player {
    fn new(_owner: &KinematicBody2D) -> Self {
        Player::default()
    }

    fn register(builder: &ClassBuilder<Self>) {
        builder.add_signal(Signal {
            name: "gravpack_fuel_change",
            args: &[SignalArgument {
                name: "fuel_left",
                default: Variant::from_f64(0.0),
                export_info: ExportInfo::new(VariantType::F64),
                usage: PropertyUsage::DEFAULT,
            }],
        });
    }

    pub fn set_settings(&mut self, settings: Rc<RefCell<Settings>>) {
        self.settings = settings;
    }

    pub fn set_net_core_acceleration(&mut self, value: Vector2) {
        self.net_core_acceleration = value;
    }

    pub fn set_velocity(&mut self, value: Vector2) {
        self.velocity = value;
    }

    pub fn set_gravpack_fuel(&mut self, value: f32) {
        self.gravpack_fuel = value;
    }

    pub fn get_gravpack_fuel(&self) -> f32 {
        self.gravpack_fuel
    }

    pub fn set_air_parent(&mut self, air_parent: Ref<Node>) {
        self.air_parent = Some(air_parent);
    }

    /// Set the angle to the victory, in a global angle.
    pub fn set_victory_angle(&self, angle: Angle<f32>) {
        let victory_arrow = unsafe { self.victory_arrow.unwrap().assume_safe() };
        victory_arrow.set_global_rotation(angle.to_f64().get());
    }

    /// Rotate a controller-oriented vector, using the settings, into global space.
    fn controller_to_global(&self, owner: &KinematicBody2D, controller: Vector2) -> Vector2 {
        let settings = self.settings.borrow();
        match settings.controls_alignment() {
            ControlsAlignment::Player => {
                controller.rotated(Angle::radians(owner.global_rotation() as f32))
            }
            ControlsAlignment::Camera => {
                let camera = unsafe { self.camera.unwrap().assume_safe() };
                controller.rotated(Angle::radians(camera.global_rotation() as f32))
            }
        }
    }

    /// Rotate a controller-oriented vector, using the settings, into player space.
    fn controller_to_player(&self, owner: &KinematicBody2D, controller: Vector2) -> Vector2 {
        let settings = self.settings.borrow();
        match settings.controls_alignment() {
            ControlsAlignment::Player => controller,
            ControlsAlignment::Camera => {
                let camera = unsafe { self.camera.unwrap().assume_safe() };
                controller.rotated(Angle::radians(
                    camera.global_rotation() as f32 - owner.global_rotation() as f32,
                ))
            }
        }
    }

    #[export]
    fn _ready(&mut self, owner: TRef<KinematicBody2D>) {
        let node: TRef<Node> = owner.upcast();
        self.floor_detector = Some(unsafe { node.get_claimed_node("FloorDetector") });
        self.sprite = Some(unsafe { node.get_claimed_node("Sprite") });
        self.victory_arrow = Some(unsafe { node.get_claimed_node("VictoryArrow") });
        self.velocity_arrow = Some(unsafe { node.get_claimed_node("VelocityArrow") });
        self.velocity_pivot = Some(unsafe { node.get_claimed_node("VelocityArrow/Pivot") });
        self.velocity_label = Some(unsafe { node.get_claimed_node("VelocityArrow/Pivot/Label") });
        self.animation_player = Some(unsafe { node.get_claimed_node("AnimationPlayer") });
        let camera: TRef<Camera2D, _> = unsafe { node.get_typed_node("CameraHolder/Camera") };
        camera.set_global_position(owner.global_position());
        camera.set_global_rotation(owner.global_rotation());
        self.camera = Some(camera.claim());

        self.gravpack_shot_reground_timeout =
            Some(unsafe { node.get_claimed_node("GravpackShotRegroundTimeout") });

        let gravpack_shot_overlay: TRef<Node2D> =
            unsafe { node.get_typed_node("GravpackShotOverlay") };
        owner.remove_child(gravpack_shot_overlay);
        gravpack_shot_overlay.set_visible(true);
        let gravpack_shot_overlay: RefInstance<GravpackShotOverlay, _> =
            gravpack_shot_overlay.cast_instance().unwrap();
        self.gravpack_shot_overlay = Some(gravpack_shot_overlay.claim().into());
    }

    #[export]
    fn _process(&self, owner: TRef<KinematicBody2D>, delta: f32) {
        let settings = self.settings.borrow();
        let camera = unsafe { self.camera.unwrap().assume_safe() };
        let weight = (settings.translation_multiplier() as f32 * delta).min(1.0);
        camera.set_global_position(
            camera
                .global_position()
                .lerp(owner.global_position(), weight as f32),
        );
        match settings.camera_type() {
            CameraType::Fixed => camera.set_global_rotation(0.0),
            CameraType::Rotating {
                rotation_multiplier,
            } => {
                let weight = (rotation_multiplier as f32 * delta).min(1.0);
                camera.set_global_rotation(
                    Angle::radians(camera.global_rotation() as f32)
                        .lerp(Angle::radians(owner.global_rotation() as f32), weight)
                        .get() as f64,
                );
            }
        }
    }

    #[export]
    fn _physics_process(&mut self, owner: TRef<KinematicBody2D>, delta: f32) {
        let input = Input::godot_singleton();
        if input.is_action_pressed("gravpack_shot") {
            unsafe {
                owner.call_deferred("gravpack_shot", &[]);
            }
        }

        let floor_detector = self.floor_detector.expect("Floor detector must be set");
        let floor_detector = unsafe { floor_detector.assume_safe() };
        let animation;

        let mut velocity = self.velocity;

        let player_up_vector;
        let on_ground = floor_detector.is_colliding();
        if on_ground {
            // Ground a player if they are ungrounded, ie. set their parent to be the new ground.
            if !self.grounded {
                self.grounded = true;
                let collider = unsafe { floor_detector.get_collider().unwrap().assume_safe() };
                let collider: TRef<Node, _> = collider.cast().unwrap();
                let prev_parent = unsafe { owner.get_parent().unwrap().assume_safe() };

                // When reparenting a node, the global transform is not preserved.  We have to do
                // this to conserve it.
                let global_transform = owner.get_global_transform();
                prev_parent.remove_child(owner);
                collider.add_child(owner, false);
                owner.set_global_transform(global_transform);
            }

            // Ground the player, setting their up angle to point away from the ground they're
            // standing on.
            player_up_vector = floor_detector.get_collision_normal();
            let up_angle = Vector2::new(0.0, -1.0).angle_to(player_up_vector);

            let horizontal_movement = input.get_action_strength("move_right") as f32
                - input.get_action_strength("move_left") as f32;
            let vertical_movement = input.get_action_strength("move_down") as f32
                - input.get_action_strength("move_up") as f32;
            let controller_movement =
                Vector2::new(horizontal_movement, vertical_movement).with_max_length(1.0);
            let relative_movement = self.controller_to_player(&owner, controller_movement);
            let jump = input.is_action_just_pressed("jump");

            // Get the player's velocity relative to their current orientation
            let mut relative_velocity = velocity.rotated(-up_angle);

            // Override the horizontal movement with the input movement.
            relative_velocity.x = relative_movement.x * self.run_speed;

            // Vertical motion needs to be added to allow for proper jumping and falling.
            if jump {
                relative_velocity.y += -self.jump_speed;
                self.in_jump = true;
            }

            velocity = relative_velocity.rotated(up_angle);

            let horizontal_magnitude = relative_movement.x.abs();
            if horizontal_magnitude > 0.01 {
                let sprite = self.sprite.expect("Sprite must be set");
                let sprite = unsafe { sprite.assume_safe() };
                sprite.set_flip_h(relative_movement.x < 0.0);
            }

            animation = if horizontal_magnitude > 0.75 {
                Animation::Run
            } else if horizontal_magnitude > 0.01 {
                Animation::Walk
            } else {
                Animation::Idle
            };
        } else {
            // Unground a player if they are grounded, ie. set their parent to be the hole.
            if self.grounded {
                self.grounded = false;
                if let Some(air_parent) = self.air_parent {
                    let prev_parent = unsafe { owner.get_parent().unwrap().assume_safe() };
                    let air_parent = unsafe { air_parent.assume_safe() };
                    let global_transform = owner.get_global_transform();
                    prev_parent.remove_child(owner);
                    air_parent.add_child(owner, false);
                    owner.set_global_transform(global_transform);
                }
            }

            player_up_vector = -self.net_core_acceleration.normalize();

            // Need some separate calculation here because the gravpack relative velocity is rotated to
            // the player's rotation.
            let horizontal_gravpack = input.get_action_strength("gravpack_right")
                - input.get_action_strength("gravpack_left");
            let vertical_gravpack = input.get_action_strength("gravpack_down")
                - input.get_action_strength("gravpack_up");
            let gravpack_controller_acceleration =
                Vector2::new(horizontal_gravpack as f32, vertical_gravpack as f32)
                    .with_max_length(1.0)
                    * self.settings.borrow().gravpack_acceleration();
            let gravpack_controller_velocity: Vector2 =
                gravpack_controller_acceleration * delta as f32;
            let gravpack_controller_velocity =
                gravpack_controller_velocity.with_max_length(self.gravpack_fuel as f32);
            if gravpack_controller_velocity.square_length() > 0.001 {
                let gravpack_global_velocity =
                    self.controller_to_global(&owner, gravpack_controller_velocity);
                velocity += gravpack_global_velocity;
                self.gravpack_fuel -= gravpack_global_velocity.length();
                owner.emit_signal(
                    "gravpack_fuel_change",
                    &[Variant::from_f64(self.gravpack_fuel as f64)],
                );
            }

            // When in the air, up is the opposite of the net pull
            let moving_up = velocity.dot(player_up_vector) > 0.0;
            animation = if moving_up {
                Animation::Jump
            } else {
                self.in_jump = false;

                Animation::Fall
            };

            // Jump cancel
            if self.in_jump && input.is_action_just_released("jump") {
                let up_angle = Vector2::new(0.0, -1.0).angle_to(player_up_vector);
                let mut relative_velocity = velocity.rotated(-up_angle);
                relative_velocity.y = 0.0;
                velocity = relative_velocity.rotated(up_angle);
                self.in_jump = false;
            }
        }

        // Though the up_angle adjusts instantly, we want player rotation to be smooth, so we lerp
        // it.
        let weight = (delta * ROTATION_MULTIPLIER).min(1.0);
        owner.set_global_rotation(
            Angle::radians(owner.global_rotation() as f32)
                .lerp(Vector2::new(0.0, -1.0).angle_to(player_up_vector), weight)
                .get() as f64,
        );

        // Need delta because the value is acceleration, not velocity.  We use a dot product so
        // that the player can't be pulled sideways while grounded.  All core pull is vertical
        // while on the ground.  When in the air, up_vector is always aligned with the
        // net_core_acceleration anyway.
        let net_core_acceleration =
            player_up_vector * self.net_core_acceleration.dot(player_up_vector);
        let net_core_velocity = net_core_acceleration * delta as f32;
        velocity += net_core_velocity;

        self.velocity = owner.move_and_slide(
            velocity,
            player_up_vector,
            true,
            4,
            f64::consts::FRAC_PI_4,
            true,
        );

        if animation != self.current_animation {
            self.current_animation = animation.into();
            let animation_player = self.animation_player.expect("Animation Player must be set");
            let animation_player = unsafe { animation_player.assume_safe() };
            animation_player.play(animation.to_str(), -1.0, 1.0, false);
        }
    }

    /// Enter the gravpack shot state, pause the tree, and enable the gravpack overlay.
    #[export]
    fn gravpack_shot(&self, owner: TRef<KinematicBody2D>) {
        let settings = self.settings.borrow();
        let tree = owner.get_tree().unwrap();
        let tree = unsafe { tree.assume_safe() };
        tree.set_pause(true);

        let camera = unsafe { self.camera.unwrap().assume_safe() };
        let camera_angle = Angle::radians(camera.global_rotation() as f32);

        let rotation = match settings.controls_alignment() {
            ControlsAlignment::Player => Angle::zero(),
            ControlsAlignment::Camera => {
                camera_angle - Angle::radians(owner.global_rotation() as f32)
            }
        };

        let gravpack_shot_overlay =
            unsafe { self.gravpack_shot_overlay.as_ref().unwrap().assume_safe() };
        gravpack_shot_overlay
            .map_mut(|script, base| {
                script.set_rotation(rotation);
                script.set_camera_rotation(camera_angle);
                script.set_value(0.0);
                script.set_max_value(self.gravpack_fuel as f32);
                script.set_sensitivity(settings.gravpack_shot_sensitivity());
                owner.add_child(base, false);
            })
            .unwrap();

        if self.velocity != Vector2::zero() {
            {
                let velocity_arrow = unsafe { self.velocity_arrow.unwrap().assume_safe() };
                velocity_arrow.set_visible(true);
                velocity_arrow
                    .set_global_rotation(self.velocity.angle_from_x_axis().to_f64().get());
            }
            {
                let velocity_label = unsafe { self.velocity_label.unwrap().assume_safe() };
                velocity_label.set_text(format!("{:.0}", self.velocity.length()));
            }
            {
                let velocity_pivot = unsafe { self.velocity_pivot.unwrap().assume_safe() };
                velocity_pivot.set_global_rotation(camera_angle.radians as f64);
            }
        }
    }

    /// Release the gravpack shot.
    ///
    /// Unpauses the tree and applies gravpack velocity.
    #[export]
    fn gravpack_shot_release(&mut self, owner: TRef<KinematicBody2D>, force: Vector2) {
        let tree = owner.get_tree().unwrap();
        let tree = unsafe { tree.assume_safe() };
        tree.set_pause(false);
        let gravpack_shot_overlay = self.gravpack_shot_overlay.as_ref().unwrap();
        owner.remove_child(gravpack_shot_overlay.base());

        let force_magnitude = force.length();
        if force_magnitude > 0.0 {
            // The strength vector is always player-local
            let force_global = force.rotated(Angle::radians(owner.global_rotation() as f32));
            self.velocity += force_global;
            self.gravpack_fuel -= force_magnitude;
            self.gravpack_fuel = self.gravpack_fuel.max(0.0);
            owner.emit_signal(
                "gravpack_fuel_change",
                &[Variant::from_f64(self.gravpack_fuel as f64)],
            );

            // Disable grounding for a timeout period, to allow the player to properly fly at whatever
            // angle they choose.
            let floor_detector = unsafe { self.floor_detector.unwrap().assume_safe() };
            floor_detector.set_enabled(false);
            let gravpack_shot_reground_timeout =
                unsafe { self.gravpack_shot_reground_timeout.unwrap().assume_safe() };
            gravpack_shot_reground_timeout.start(0.0);
        }

        let velocity_arrow = unsafe { self.velocity_arrow.unwrap().assume_safe() };
        velocity_arrow.set_visible(false);
    }

    /// Enables the floor detector.
    ///
    /// Exported so the gravpack shot timer can do it with a signal.
    #[export]
    fn enable_floor_detector(&mut self, _owner: TRef<KinematicBody2D>) {
        let floor_detector = unsafe { self.floor_detector.unwrap().assume_safe() };
        floor_detector.set_enabled(true);
    }
}
